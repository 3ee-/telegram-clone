package com.beebee.telegram_clone.model.pojo

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Chat(
	val fullname: String,
	val text: String,
	val image: String,
	val readFlag: Boolean = false,
	val parentMessagesID: String,
	val lastMessagesID: String,
	val senderID: String,
	val recepientID: String,
	val recepientName: String,
	val createdAt: Timestamp
) : Parcelable