package com.beebee.telegram_clone.model.pojo

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
	val uid: String? = null,
	val fullname: String,
	val username: String,
	val email: String,
	val password: String,
	val image: String? = "https://via.placeholder.com/100"
): Parcelable