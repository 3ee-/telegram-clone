package com.beebee.telegram_clone.model.services.auth

import android.util.Log
import com.beebee.telegram_clone.constants.USER_COLLECTIONS
import com.beebee.telegram_clone.model.pojo.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import javax.inject.Inject

class Auth @Inject constructor() : IAuth {
	override fun registerUser(user: User, response: (String) -> Unit) {
		FirebaseAuth.getInstance().createUserWithEmailAndPassword(user.email, user.password)
			.addOnSuccessListener { result ->
				// User data
				val data = HashMap<String, String>()
				data.put("fullname", user.fullname)
				data.put("username", user.username)
				data.put("email", user.email)
				data.put("password", user.password)

				val profileChangeRequest = UserProfileChangeRequest.Builder().setDisplayName(user.fullname).build()
				FirebaseAuth.getInstance().currentUser?.updateProfile(profileChangeRequest)

				// Save user to firestore
				FirebaseFirestore.getInstance().collection(USER_COLLECTIONS)
					.document(result.user!!.uid)
					.set(data)
					.addOnSuccessListener { document ->
						Log.d("Firebase", "Successfully Registered")
						response.invoke("Successfully Registered")
					}
					.addOnFailureListener { exception ->
						Log.e("Hello", exception.localizedMessage)
						response.invoke("Error Registered: ${exception.localizedMessage}")
					}
			}
			.addOnFailureListener { exception ->
				response.invoke("Error Registered: ${exception.localizedMessage}")
			}
	}

	override fun loginuser(email: String, password: String, response: Response) {
		FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
			.addOnSuccessListener {
				response.invoke("Login Successfully")
			}
			.addOnFailureListener { exception ->
				response.invoke("Error Login: ${exception.localizedMessage}")
			}
	}
}