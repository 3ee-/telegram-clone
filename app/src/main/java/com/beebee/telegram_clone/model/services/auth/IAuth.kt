package com.beebee.telegram_clone.model.services.auth

import com.beebee.telegram_clone.model.pojo.User

typealias Response = (String) -> Unit

interface IAuth {
	fun registerUser(user: User, response: Response)
	fun loginuser(email: String, password: String, response: Response)
}