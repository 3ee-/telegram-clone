package com.beebee.telegram_clone.model.services.detail

import android.util.Log
import com.beebee.telegram_clone.model.pojo.Chat
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.Exception
import javax.inject.Inject

class DetailServices @Inject constructor() : IDetailServices {
	val firestore = FirebaseFirestore.getInstance()

	override fun getListsChats(parentMessagesID: String, response: (List<Chat>) -> Unit) {
		firestore.collection("chats")
			.get()
			.addOnSuccessListener { documents ->
				val listChats: MutableList<Chat> = mutableListOf()
				for (document in documents) {
					if (document.get("parentMessagesID").toString() == parentMessagesID) {
						val chat = Chat(
							fullname = document.get("fullname").toString(),
							text = document.get("text").toString(),
							image = document.get("image").toString(),
							readFlag = document.get("readFlag").toString().toBoolean(),
							parentMessagesID = document.get("parentMessagesID").toString(),
							lastMessagesID = document.get("lastMessagesID").toString(),
							senderID = document.get("senderID").toString(),
							recepientID = document.get("recepientID").toString(),
							recepientName = document.get("recepientName").toString(),
							createdAt = Timestamp(document.getDate("createdAt")!!)
						)
						listChats.add(chat)
					}
				}
				val sortedLists = listChats.sortedWith(compareBy { it.createdAt })
				response.invoke(sortedLists)
			}
			.addOnFailureListener {
				Log.d("Errors: ", it.localizedMessage)
			}
	}

	override fun getRealtimeChats(parentMessagesID: String, response: GetSingleResponse) {
		firestore.collection("chats")
			.addSnapshotListener { snapshot, exception ->
				try {
					val documentChange = snapshot?.documentChanges!!.last()
					when (documentChange.type) {
						DocumentChange.Type.ADDED -> {
							if (documentChange.document.get("parentMessagesID").toString() == parentMessagesID) {
								val chatResponse = Chat(
									fullname = documentChange.document.get("fullname").toString(),
									text = documentChange.document.get("text").toString(),
									image = documentChange.document.get("image").toString(),
									readFlag = documentChange.document.get("readFlag").toString().toBoolean(),
									parentMessagesID = documentChange.document.get("parentMessagesID").toString(),
									lastMessagesID = documentChange.document.get("lastMessagesID").toString(),
									senderID = documentChange.document.get("senderID").toString(),
									recepientID = documentChange.document.get("recepientID").toString(),
									recepientName = documentChange.document.get("recepientName").toString(),
									createdAt = Timestamp(documentChange.document.getDate("createdAt")!!)
								)
								response.invoke(chatResponse)
							}
						}
					}
				} catch (e: Exception) {
					Log.e("Errors:", exception?.localizedMessage)
				}
			}
	}

	override fun sendPrivateMessages(chat: Chat) {
		firestore.collection("chats").document(chat.lastMessagesID)
			.update("lastMessagesID", "")
			.addOnSuccessListener {
				firestore.collection("chats")
					.add(chat)
					.addOnSuccessListener { document ->
						firestore.collection("chats").document(chat.parentMessagesID)
							.update("lastMessagesID", document.id)
							.addOnFailureListener {
								Log.e("Errors: ", it.localizedMessage)
							}
							Log.d("Docs ID: ", document.id)

						firestore.collection("chats").document(document.id)
							.update("lastMessagesID", document.id)
							.addOnSuccessListener {
								firestore.collection("chats").document(document.id)
									.get()
									.addOnSuccessListener { }
									.addOnFailureListener {
										Log.e("Errors: ", it.localizedMessage)
									}
							}
							.addOnFailureListener {
								Log.e("Errors: ", it.localizedMessage)
							}
					}
					.addOnFailureListener {
						Log.e("Errors: ", it.localizedMessage)
					}
			}
			.addOnFailureListener {
				Log.e("Errors: ", it.localizedMessage)
			}
	}
}