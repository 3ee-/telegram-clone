package com.beebee.telegram_clone.model.services.detail

import com.beebee.telegram_clone.model.pojo.Chat

typealias GetListResponse = (List<Chat>) -> Unit
typealias GetSingleResponse = (Chat) -> Unit

interface IDetailServices {
	fun getListsChats(parentMessagesID: String, response: GetListResponse)
	fun getRealtimeChats(parentMessagesID: String, response: GetSingleResponse)
	fun sendPrivateMessages(chat: Chat)
}