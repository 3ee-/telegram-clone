package com.beebee.telegram_clone.model.services.list

import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.model.pojo.User

interface IListServices {
	fun fetchUsers(response: (List<User>) -> Unit)
	fun fetchChats(response: (List<Chat>) -> Unit)
}