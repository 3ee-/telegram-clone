package com.beebee.telegram_clone.model.services.list

import android.util.Log
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.model.pojo.User
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import javax.inject.Inject

class ListServices @Inject constructor(): IListServices {
	val auth = FirebaseAuth.getInstance()

	override fun fetchUsers(response: (List<User>) -> Unit) {
		FirebaseFirestore.getInstance().collection("users")
			.get()
			.addOnSuccessListener { documents ->
				val listData: MutableList<User> = mutableListOf()
				for (document in documents) {
					if (document.id != auth.uid) {
						listData.add(
							User(
								document.id.toString(),
								document.data.getValue("fullname").toString(),
								document.data.getValue("username").toString(),
								document.data.getValue("email").toString(),
								document.data.getValue("password").toString()
							)
						)
					}
				}
				response.invoke(listData)
			}
			.addOnFailureListener {
				Log.d("Errors: ", it.localizedMessage)
			}
	}

	override fun fetchChats(response: (List<Chat>) -> Unit) {
		FirebaseFirestore.getInstance().collection("chats")
			.get()
			.addOnSuccessListener { documents ->
				val listChats: MutableList<Chat> = mutableListOf()
				for (document in documents) {
					if (document.get("senderID") == auth.currentUser!!.uid || document.get("recepientID") == auth.currentUser!!.uid) {
						if (document.id == document.get("lastMessagesID")) {
							var fullname = document.get("fullname").toString()
							if (document.get("senderID") == auth.currentUser!!.uid) {
								fullname = document.get("recepientName").toString()
							}
							listChats.add(
								Chat(
									fullname = fullname,
									text = document.get("text").toString(),
									image = document.get("image").toString(),
									readFlag = document.get("readFlag").toString().toBoolean(),
									parentMessagesID = document.get("parentMessagesID").toString(),
									lastMessagesID = document.get("lastMessagesID").toString(),
									senderID = document.get("senderID").toString(),
									recepientID = document.get("recepientID").toString(),
									recepientName = document.get("recepientName").toString(),
									createdAt = Timestamp(document.getDate("createdAt")!!)
								)
							)
						}
					}
				}
				response.invoke(listChats)
			}
			.addOnFailureListener {
				Log.d("Errors: ", it.localizedMessage)
			}
	}
}