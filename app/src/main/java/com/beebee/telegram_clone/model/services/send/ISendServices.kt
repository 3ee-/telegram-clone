package com.beebee.telegram_clone.model.services.send

import com.beebee.telegram_clone.model.pojo.Chat

interface ISendServices {
	fun sendMessages(chat: Chat, response: (String) -> Unit)
}