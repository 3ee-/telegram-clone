package com.beebee.telegram_clone.model.services.send

import android.util.Log
import com.beebee.telegram_clone.model.pojo.Chat
import com.google.firebase.firestore.FirebaseFirestore
import javax.inject.Inject

class SendServices @Inject constructor(): ISendServices {
	private val firestore = FirebaseFirestore.getInstance()
	override fun sendMessages(chat: Chat, response: (String) -> Unit) {
		firestore.collection("chats")
			.add(chat)
			.addOnSuccessListener { document ->
				firestore.collection("chats").document(document.id)
					.update("lastMessagesID", document.id)
				firestore.collection("chats").document(document.id)
					.update("parentMessagesID", document.id)
					.addOnSuccessListener {
						response.invoke("Successfully Sent")
					}
					.addOnFailureListener {
						Log.e("Error", it.localizedMessage)
					}
			}
			.addOnFailureListener {
				Log.e("Error", it.localizedMessage)
			}
	}
}