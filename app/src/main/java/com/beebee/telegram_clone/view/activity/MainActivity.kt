package com.beebee.telegram_clone.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.beebee.telegram_clone.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
