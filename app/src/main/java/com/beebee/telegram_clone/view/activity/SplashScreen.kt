package com.beebee.telegram_clone.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.utils.AuthHelper
import java.util.*
import kotlin.concurrent.schedule

class SplashScreen : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_splash_screen)

		finishSplashScreen()
	}

	private fun finishSplashScreen() {
		Timer().schedule(2000) {
			var intent = Intent()
			if (!AuthHelper.checkAuth()) {
				intent = Intent(this@SplashScreen, AuthActivity::class.java)
			} else {
				intent = Intent(this@SplashScreen, MainActivity::class.java)
			}
			startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
			finish()
		}
	}
}
