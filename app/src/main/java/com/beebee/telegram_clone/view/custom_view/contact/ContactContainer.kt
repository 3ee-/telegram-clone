package com.beebee.telegram_clone.view.custom_view.contact

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.beebee.telegram_clone.model.pojo.User
import com.beebee.telegram_clone.view.ui.contact.ContactAdapter
import com.beebee.telegram_clone.viewmodel.list.ListViewModel
import kotlinx.android.synthetic.main.detail_navbar.view.*
import kotlinx.android.synthetic.main.fragment_contact.view.*

class ContactContainer @JvmOverloads constructor(
	context: Context,
	attrs: AttributeSet? = null,
	defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
	private lateinit var adapter: ContactAdapter

	fun initView(viewModel: ListViewModel, lifecycleOwner: LifecycleOwner) {
		nav_back.setOnClickListener {
			findNavController().popBackStack()
		}

		renderContent()
		viewModel.fetchUsers()
		observeViewModel(viewModel, lifecycleOwner)
	}

	private fun renderContent() {
		list_users.layoutManager = LinearLayoutManager(context)
		adapter = ContactAdapter()
		list_users.adapter = adapter
	}

	private fun observeViewModel(viewModel: ListViewModel, lifecycleOwner: LifecycleOwner) {
		viewModel.listUsers.observe(lifecycleOwner, Observer { users ->
			updateUsers(users)
		})
	}

	private fun updateUsers(users: List<User>) {
		adapter.updateUsers(users)
	}
}