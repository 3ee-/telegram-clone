package com.beebee.telegram_clone.view.custom_view.contact

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.navigation.findNavController
import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.model.pojo.User
import com.beebee.telegram_clone.view.ui.contact.ContactFragmentDirections
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_users.view.*

class ContactItemContainer @JvmOverloads constructor(
	context: Context,
	attrs: AttributeSet? = null,
	defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
	fun initView(user: User) {
		user.image.let { imageUri ->
			Glide.with(this)
				.load(imageUri)
				.placeholder(R.mipmap.telegram_logo)
				.into(profile_image)
		}

		user.fullname.let { fullname ->
			profile_name.text = fullname
		}

		this.setOnClickListener {
			val action = ContactFragmentDirections.actionContactFragmentToSendFragment(user)
			findNavController().navigate(action)
		}
	}
}