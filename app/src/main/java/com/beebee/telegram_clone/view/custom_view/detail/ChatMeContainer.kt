package com.beebee.telegram_clone.view.custom_view.detail

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.beebee.telegram_clone.model.pojo.Chat
import kotlinx.android.synthetic.main.row_chats_me.view.*

class ChatMeContainer @JvmOverloads constructor(
	context: Context,
	attrs: AttributeSet? = null,
	defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
	fun initView(chat: Chat) {
		text_me.text = chat.text
	}
}