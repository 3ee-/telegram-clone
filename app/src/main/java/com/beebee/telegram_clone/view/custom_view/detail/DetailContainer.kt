package com.beebee.telegram_clone.view.custom_view.detail

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.utils.AuthHelper
import com.beebee.telegram_clone.view.ui.detail.DetailChatAdapter
import com.beebee.telegram_clone.viewmodel.detail.DetailViewModel
import com.google.firebase.Timestamp
import kotlinx.android.synthetic.main.detail_navbar.view.*
import kotlinx.android.synthetic.main.fragment_detail.view.*
import java.time.Instant
import java.util.*

class DetailContainer @JvmOverloads constructor(
	context: Context,
	attrs: AttributeSet? = null,
	defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
	private lateinit var adapter: DetailChatAdapter

	fun initView(chat: Chat, viewModel: DetailViewModel, lifecycleOwner: LifecycleOwner) {
		nav_back.setOnClickListener {
			findNavController().popBackStack()
		}

		send_button.setOnClickListener { sendMessages(chat, viewModel) }

		renderContent(chat)
		viewModel.getListsChat(chat.parentMessagesID)
		viewModel.getRealTimeChat(chat.parentMessagesID)
		observeViewModel(viewModel, lifecycleOwner)
	}

	private fun renderContent(chat: Chat) {
		detail_profile_name.text = chat.fullname

		adapter = DetailChatAdapter()
		detail_chat_container.layoutManager = LinearLayoutManager(context)
		detail_chat_container.adapter = adapter
	}

	private fun observeViewModel(viewModel: DetailViewModel, lifecycleOwner: LifecycleOwner) {
		viewModel.listChats.observe(lifecycleOwner, Observer {
			updateLists(it)
			detail_chat_container.scrollToPosition(it.size - 1)
			chat_input.setText("")
		})
	}

	private fun updateLists(list: List<Chat>) {
		adapter.updateList(list)
	}

	@SuppressLint("NewApi")
	private fun sendMessages(chat: Chat, viewModel: DetailViewModel) {
		val inputMessages = chat_input.text.toString()
		val user = AuthHelper.authFirebase.currentUser

		if (inputMessages.isNullOrEmpty()) return

		var recepientID = ""

		if (chat.senderID == user?.uid) {
			recepientID = chat.recepientID
		} else {
			recepientID = chat.senderID
		}

		val chatSend = Chat (
			fullname = user?.displayName.toString(),
			text = inputMessages,
			image = chat.image,
			readFlag = false,
			parentMessagesID = chat.parentMessagesID,
			lastMessagesID = chat.lastMessagesID,
			senderID = AuthHelper.authFirebase.currentUser?.uid.toString(),
			recepientID = recepientID,
			recepientName = chat.recepientName,
			createdAt = Timestamp(Date.from(Instant.now()))
		)

		viewModel.sendPrivateMessage(chatSend)
	}
}