package com.beebee.telegram_clone.view.custom_view.list

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.view.ui.list.ListAdapter
import com.beebee.telegram_clone.view.ui.list.ListFragmentDirections
import com.beebee.telegram_clone.viewmodel.list.ListViewModel
import kotlinx.android.synthetic.main.fragment_list.view.*

class ListContainer @JvmOverloads constructor(
	context: Context,
	attrs: AttributeSet? = null,
	defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
	private lateinit var adapter: ListAdapter

	fun initView(actionDelegate: ListViewModel, lifecycleOwner: LifecycleOwner) {
		adapter = ListAdapter()
		contact_container.layoutManager = LinearLayoutManager(context)
		contact_container.adapter = adapter
		actionDelegate.fetchChats()
		observeViewModel(actionDelegate, lifecycleOwner)

		add_message.setOnClickListener {
			val action = ListFragmentDirections.actionListFragmentToContactFragment()
			findNavController().navigate(action)
		}
	}

	private fun observeViewModel(actionDelegate: ListViewModel, lifecycleOwner: LifecycleOwner) {
		actionDelegate.listChats.observe(lifecycleOwner, Observer { chats ->
			updateList(chats)
		})
	}

	fun updateList(userList: List<Chat>) {
		adapter.updateLists(userList)
	}
}