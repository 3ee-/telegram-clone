package com.beebee.telegram_clone.view.custom_view.list

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.navigation.findNavController
import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.view.ui.list.ListFragmentDirections
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_users.view.*

class ListItemContainer @JvmOverloads constructor(
	context: Context,
	attrs: AttributeSet? = null,
	defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
	fun initView(chat: Chat) {
		chat.image.let { imageUri ->
			Glide.with(this)
				.load(imageUri)
				.placeholder(R.mipmap.telegram_logo)
				.into(profile_image)
		}
		chat.fullname.let { fullname ->
			profile_name.text = fullname
		}

		this.setOnClickListener {
			val action = ListFragmentDirections.actionListFragmentToDetailFragment(chat)
			findNavController().navigate(action)
		}
	}
}