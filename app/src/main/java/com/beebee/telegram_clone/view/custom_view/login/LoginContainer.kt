package com.beebee.telegram_clone.view.custom_view.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.utils.AuthHelper
import com.beebee.telegram_clone.view.activity.MainActivity
import com.beebee.telegram_clone.view.ui.login.LoginFragmentDirections
import com.beebee.telegram_clone.viewmodel.login.LoginViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_login.view.*

class LoginContainer @JvmOverloads constructor(
	context: Context,
	attrs: AttributeSet? = null,
	defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
	fun initView(actionDelegate: LoginViewModel, lifecycleOwner: LifecycleOwner) {
		move_to_signup.setOnClickListener {
			val action = LoginFragmentDirections.actionLoginFragmentToSignUpFragment()
			findNavController().navigate(action)
		}

		login_button.setOnClickListener { onLogin(actionDelegate) }
		observeViewModel(actionDelegate, lifecycleOwner)
	}

	private fun onLogin(actionDelegate: LoginViewModel) {
		val emailData = email.text.toString()
		val passwordData = password.text.toString()

		val validate = validation(emailData, passwordData)

		if (!validate) {
			val errorMessage = "All field must be filled!"
			error_login.text = errorMessage
			error_login.visibility = View.VISIBLE
			return
		}

		login_button.isClickable = false
		progress_login.visibility = View.VISIBLE
		error_login.visibility = View.GONE
		error_login.text = ""

		actionDelegate.loginUser(
			email = emailData,
			password = AuthHelper.hashPassword(passwordData).toString()
		)
	}

	private fun moveActivity() {
		val activity = context as Activity
		val intent = Intent(context, MainActivity::class.java)
		context.startActivity(intent)
		activity.finish()
	}

 	private fun observeViewModel(actionDelegate: LoginViewModel, lifecycleOwner: LifecycleOwner) {
	  actionDelegate.responseLiveData.observe(lifecycleOwner, Observer { response ->
		  login_button.isClickable = true
		  progress_login.visibility = View.GONE

		  Snackbar.make(this, response, Snackbar.LENGTH_LONG)
			  .setAction("Close") { }
			  .setBackgroundTint(resources.getColor(R.color.colorPrimaryDark))
			  .setTextColor(resources.getColor(R.color.colorPrimary))
			  .setActionTextColor(resources.getColor(R.color.colorAccent))
			  .show()

		  Log.d("res", response)

		  if (response == "Login Successfully") {
			  email.setText("")
			  password.setText("")
			  moveActivity()
		  }
	  })
  }

	private fun validation(email: String, password: String): Boolean {
		if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
			return false
		}
		return true
	}
}
