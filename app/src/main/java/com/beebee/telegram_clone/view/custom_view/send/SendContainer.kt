package com.beebee.telegram_clone.view.custom_view.send

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.model.pojo.User
import com.beebee.telegram_clone.utils.AuthHelper
import com.beebee.telegram_clone.viewmodel.send.SendViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Timestamp
import kotlinx.android.synthetic.main.detail_navbar.view.*
import kotlinx.android.synthetic.main.fragment_send.view.*
import java.time.Instant
import java.util.*

class SendContainer @JvmOverloads constructor(
	context: Context,
	attrs: AttributeSet? = null,
	defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
	fun initView(user: User, viewModel: SendViewModel, lifecycleOwner: LifecycleOwner) {
		nav_back.setOnClickListener {
			findNavController().popBackStack()
		}

		add_message.setOnClickListener { sendMessages(user, viewModel) }
		detail_profile_name.text = user.fullname

		observeViewModel(viewModel, lifecycleOwner)
	}

	private fun observeViewModel(viewModel: SendViewModel, lifecycleOwner: LifecycleOwner) {
		viewModel.responseMessages.observe(lifecycleOwner, Observer { response ->
			send_field.setText("")

			Snackbar.make(this, response, Snackbar.LENGTH_LONG)
				.setAction("Close") { }
				.setBackgroundTint(resources.getColor(R.color.colorPrimaryDark))
				.setTextColor(resources.getColor(R.color.colorPrimary))
				.setActionTextColor(resources.getColor(R.color.colorAccent))
				.show()

			findNavController().popBackStack()
		})
	}

	@SuppressLint("NewApi")
	private fun sendMessages(user: User, viewModel: SendViewModel) {
		val sendField = send_field.text.toString()
		val firebaseUser = AuthHelper.authFirebase.currentUser

		val sendChat = Chat(
			fullname = firebaseUser?.displayName.toString(),
			text = sendField,
			image = user.image.toString(),
			readFlag = false,
			parentMessagesID = "",
			lastMessagesID = "",
			senderID = AuthHelper.authFirebase.currentUser?.uid.toString(),
			recepientID = user.uid.toString(),
			recepientName = user.fullname,
			createdAt = Timestamp(Date.from(Instant.now()))
		)

		viewModel.sendMessages(sendChat)
	}
}