package com.beebee.telegram_clone.view.custom_view.signup

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.model.pojo.User
import com.beebee.telegram_clone.utils.AuthHelper
import com.beebee.telegram_clone.viewmodel.signup.SignupViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_sign_up.view.*

class SignupContainer @JvmOverloads constructor(
	context: Context,
	attrs: AttributeSet? = null,
	defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
	fun initView(actionDelegate: SignupViewModel, lifecycleOwner: LifecycleOwner) {
		move_to_login.setOnClickListener {
			findNavController().popBackStack()
		}

		register_button.setOnClickListener { onRegister(actionDelegate) }
		observeViewModel(actionDelegate, lifecycleOwner)
	}

	private fun onRegister(actionDelegate: SignupViewModel) {
		val fullnameData = fullname.text.toString()
		val usernameData = username.text.toString()
		val emailData = email.text.toString()
		val passwordData = password.text.toString()

		val validatingInput = validation(fullnameData, usernameData, emailData, passwordData)
		if (!validatingInput) {
			val errorMessage = "All field must be filled!"
			error_signup.visibility = View.VISIBLE
			error_signup.text = errorMessage
			return
		}

		register_button.isClickable = false
		progress_signup.visibility = View.VISIBLE
		error_signup.visibility = View.GONE
		error_signup.text = ""
		val user = User(
			fullname = fullnameData,
			username = usernameData,
			email = emailData,
			password = AuthHelper.hashPassword(passwordData).toString()
		)
		actionDelegate.registerUser(user)
	}

	private fun observeViewModel(actionDelegate: SignupViewModel, lifecycleOwner: LifecycleOwner) {
		actionDelegate.responseLiveData.observe(lifecycleOwner, Observer { response ->
			register_button.isClickable = true
			progress_signup.visibility = View.GONE

			Snackbar.make(this, response, Snackbar.LENGTH_LONG)
				.setAction("Close") { }
				.setBackgroundTint(resources.getColor(R.color.colorPrimaryDark))
				.setTextColor(resources.getColor(R.color.colorPrimary))
				.setActionTextColor(resources.getColor(R.color.colorAccent))
				.show()

			fullname.setText("")
			username.setText("")
			email.setText("")
			password.setText("")
			findNavController().popBackStack()
		})
	}

	private fun validation(fullnameData: String, usernameData: String, emailData: String, passwordData: String): Boolean {
		if (
			fullnameData.isNullOrEmpty()
			|| usernameData.isNullOrEmpty()
			|| emailData.isNullOrEmpty()
			|| passwordData.isNullOrEmpty()
		) {
			return false
		}
		return true
	}
}