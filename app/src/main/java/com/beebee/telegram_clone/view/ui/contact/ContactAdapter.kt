package com.beebee.telegram_clone.view.ui.contact

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.model.pojo.User
import com.beebee.telegram_clone.view.custom_view.contact.ContactItemContainer

class ContactAdapter (
	val users: MutableList<User> = mutableListOf()
) : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {
	inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
		fun bind(user: User) {
			(view as ContactItemContainer).apply {
				initView(user)
			}
		}
	}

	fun updateUsers(newUsers: List<User>) {
		users.clear()
		users.addAll(newUsers)
		notifyDataSetChanged()
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactAdapter.ViewHolder {
		val view = LayoutInflater.from(parent.context).inflate(R.layout.row_users, parent, false)
		return ViewHolder(view)
	}

	override fun getItemCount(): Int = users.size

	override fun onBindViewHolder(holder: ContactAdapter.ViewHolder, position: Int) {
		holder.bind(users[position])
	}
}