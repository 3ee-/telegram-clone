package com.beebee.telegram_clone.view.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.view.custom_view.detail.ChatContainer
import com.beebee.telegram_clone.view.custom_view.detail.ChatMeContainer
import com.google.firebase.auth.FirebaseAuth

class DetailChatAdapter(
	val chats: MutableList<Chat> = mutableListOf()
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
	val auth = FirebaseAuth.getInstance()

	companion object {
		const val TYPE_ME = 1
		const val TYPE_OTHER = 2
	}

	class OtherViewHolder(val view: View): RecyclerView.ViewHolder(view) {
		fun bind(chat: Chat) {
			(view as ChatContainer).apply {
				initView(chat)
			}
		}
	}

	class MeViewHolder(val view: View): RecyclerView.ViewHolder(view) {
		fun bind(chat: Chat) {
			(view as ChatMeContainer).apply {
				initView(chat)
			}
		}
	}

	fun updateList(newList: List<Chat>) {
		chats.clear()
		chats.addAll(newList)
		notifyDataSetChanged()
	}

	override fun getItemViewType(position: Int): Int {
		if (chats.get(position).senderID == auth.currentUser!!.uid) {
			return TYPE_ME
		} else {
			return TYPE_OTHER
		}
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
		val view: View
		if (viewType == TYPE_OTHER) {
			view = LayoutInflater.from(parent.context).inflate(R.layout.row_chats_other, parent, false)
			return OtherViewHolder(view)
		} else {
			view = LayoutInflater.from(parent.context).inflate(R.layout.row_chats_me, parent, false)
			return MeViewHolder(view)
		}
	}

	override fun getItemCount(): Int = chats.size

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
		if (getItemViewType(position) == TYPE_OTHER) {
			(holder as OtherViewHolder).bind(chats[position])
		} else  {
			(holder as MeViewHolder).bind(chats[position])
		}
	}

}