package com.beebee.telegram_clone.view.ui.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.model.pojo.User
import com.beebee.telegram_clone.view.custom_view.detail.DetailContainer
import com.beebee.telegram_clone.viewmodel.detail.DetailViewModel

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment() {
	private val args: DetailFragmentArgs by navArgs()
	private lateinit var contentView: DetailContainer
	private val viewModel: DetailViewModel
		get() = ViewModelProvider(this).get(DetailViewModel::class.java)

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.fragment_detail, container, false).apply {
			contentView = this as DetailContainer
		}
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		val chat = args.chat

		renderContent(chat)
	}

	private fun renderContent(chat: Chat) {
		contentView.initView(chat, viewModel, viewLifecycleOwner)
	}
}
