package com.beebee.telegram_clone.view.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.view.custom_view.list.ListItemContainer

class ListAdapter(
	val chats: MutableList<Chat> = mutableListOf()
) : RecyclerView.Adapter<ListAdapter.ViewHolder>() {
	inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
			fun bind(chat: Chat) {
				(view as ListItemContainer).apply {
					initView(chat)
				}
			}
	}

	fun updateLists(newList: List<Chat>) {
		chats.clear()
		chats.addAll(newList)
		notifyDataSetChanged()
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListAdapter.ViewHolder {
		val view = LayoutInflater.from(parent.context).inflate(R.layout.row_users_chats, parent, false)
		return ViewHolder(view)
	}

	override fun getItemCount(): Int = chats.size

	override fun onBindViewHolder(holder: ListAdapter.ViewHolder, position: Int) {
		holder.bind(chats[position])
	}
}