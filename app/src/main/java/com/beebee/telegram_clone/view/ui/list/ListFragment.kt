package com.beebee.telegram_clone.view.ui.list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider

import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.view.custom_view.list.ListContainer
import com.beebee.telegram_clone.viewmodel.list.ListViewModel

/**
 * A simple [Fragment] subclass.
 */
class ListFragment : Fragment() {
	private lateinit var contentView: ListContainer
	private val viewModel: ListViewModel
		get() = ViewModelProvider(this).get(ListViewModel::class.java)

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_list, container, false).apply {
			contentView = this as ListContainer
		}
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		renderContent()
	}

	private fun renderContent() {
		contentView.initView(viewModel, viewLifecycleOwner)
	}
}
