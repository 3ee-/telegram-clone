package com.beebee.telegram_clone.view.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider

import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.view.custom_view.login.LoginContainer
import com.beebee.telegram_clone.viewmodel.login.LoginViewModel

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {
	private lateinit var contentView: LoginContainer
	private val viewModel: LoginViewModel
		get() = ViewModelProvider(this).get(LoginViewModel::class.java)

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_login, container, false).apply {
			contentView = this as LoginContainer
		}
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		renderContent()
	}

	private fun renderContent() {
		contentView.initView(viewModel, viewLifecycleOwner)
	}
}
