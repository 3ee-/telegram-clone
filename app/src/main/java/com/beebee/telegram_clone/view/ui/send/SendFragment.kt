package com.beebee.telegram_clone.view.ui.send

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs

import com.beebee.telegram_clone.R
import com.beebee.telegram_clone.model.pojo.User
import com.beebee.telegram_clone.view.custom_view.send.SendContainer
import com.beebee.telegram_clone.viewmodel.send.SendViewModel

/**
 * A simple [Fragment] subclass.
 */
class SendFragment : Fragment() {
	private val args: SendFragmentArgs by navArgs()
	private lateinit var contentView: SendContainer
	private val viewModel: SendViewModel
		get() = ViewModelProvider(this).get(SendViewModel::class.java)

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_send, container, false).apply {
			contentView = this as SendContainer
		}
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		renderContent(args?.user)
	}

	private fun renderContent(user: User) {
		contentView.initView(user, viewModel, viewLifecycleOwner)
	}
}
