package com.beebee.telegram_clone.viewmodel.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.model.services.detail.DetailServices
import toothpick.Toothpick
import javax.inject.Inject

class DetailViewModel : ViewModel() {
	@Inject
	lateinit var detailServices: DetailServices

	private val _listChats: MutableLiveData<List<Chat>> = MutableLiveData()
	private val mutableListChats: MutableList<Chat> = mutableListOf()
	val listChats = _listChats

	init {
		val scope = Toothpick.openScope(this)
		Toothpick.inject(this, scope)
	}

	fun getListsChat(parentMessagesID: String) {
		detailServices.getListsChats(parentMessagesID) {
			_listChats.postValue(it)
			mutableListChats.addAll(it)
		}
	}

	fun sendPrivateMessage(chat: Chat) {
		detailServices.sendPrivateMessages(chat)
	}

	fun getRealTimeChat(parentMessagesID: String) {
		detailServices.getRealtimeChats(parentMessagesID) { chat ->
			mutableListChats.add(chat)
			_listChats.postValue(mutableListChats)
		}
	}
}