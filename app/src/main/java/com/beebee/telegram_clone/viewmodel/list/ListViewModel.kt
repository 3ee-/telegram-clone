package com.beebee.telegram_clone.viewmodel.list

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.model.pojo.User
import com.beebee.telegram_clone.model.services.list.ListServices
import toothpick.Toothpick
import javax.inject.Inject

class ListViewModel : ViewModel() {
	@Inject
	lateinit var listServices: ListServices
	private val _listUsers = MutableLiveData<List<User>>()
	private val _listChats = MutableLiveData<List<Chat>>()
	val listUsers = _listUsers
	val listChats = _listChats

	init {
		val scope = Toothpick.openScope(this)
		Toothpick.inject(this, scope)
	}

	fun fetchUsers() {
		listServices.fetchUsers() { users ->
			_listUsers.postValue(users)
		}
	}

	fun fetchChats() {
		listServices.fetchChats() { chats ->
			_listChats.postValue(chats)
		}
	}
}