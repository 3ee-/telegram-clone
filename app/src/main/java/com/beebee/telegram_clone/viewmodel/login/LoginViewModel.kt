package com.beebee.telegram_clone.viewmodel.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beebee.telegram_clone.model.services.auth.Auth
import toothpick.Toothpick
import javax.inject.Inject

class LoginViewModel : ViewModel() {
	@Inject
	lateinit var authServices: Auth
	private val _responseLiveData = MutableLiveData<String>()
	val responseLiveData = _responseLiveData

	init {
		val scope = Toothpick.openScope(this)
		Toothpick.inject(this, scope)
	}

	fun loginUser(email: String, password: String) {
		authServices.loginuser(email, password) { response ->
			_responseLiveData.postValue(response)
		}
	}
}