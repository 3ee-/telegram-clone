package com.beebee.telegram_clone.viewmodel.send

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beebee.telegram_clone.model.pojo.Chat
import com.beebee.telegram_clone.model.services.send.SendServices
import toothpick.Toothpick
import javax.inject.Inject

class SendViewModel : ViewModel() {
	@Inject
	lateinit var sendServices: SendServices
	private val _responseMessages: MutableLiveData<String> = MutableLiveData()
	val responseMessages = _responseMessages

	init {
		val scope = Toothpick.openScope(this)
		Toothpick.inject(this, scope)
	}

	fun sendMessages(chat: Chat) {
		sendServices.sendMessages(chat) { response ->
			_responseMessages.postValue(response)
		}
	}
}