package com.beebee.telegram_clone.viewmodel.signup

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beebee.telegram_clone.model.pojo.User
import com.beebee.telegram_clone.model.services.auth.Auth
import toothpick.Toothpick
import javax.inject.Inject

class SignupViewModel : ViewModel() {
	@Inject
	lateinit var authServices: Auth
	private val _responseLiveData = MutableLiveData<String>()
	val responseLiveData = _responseLiveData

	init {
		val scope = Toothpick.openScope(this)
		Toothpick.inject(this, scope)
	}

	fun registerUser(user: User) {
		authServices.registerUser(user) { response ->
			Log.d("Firebase", response)
			_responseLiveData.postValue(response)
		}
	}
}